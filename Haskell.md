# Haskell

> Written from [this book](http://learnyouahaskell.com/starting-out).

## Starting out

> To change prompt, do ``:set prompt "ghci> "``.

### Simple arithmetic

```hs
2 + 15
49 * 100
5 / 2
```

Using parenthesis:

```hs
(50 * 100) - 4999
```

### Booleans

```hs
True
False
True && True
True || False
not (True && False)
"hello" == "hello"
```

You can't mix types like ``5 + "llama"``.

### Functions

In Haskell, functions are called using the function name and the parameters just after: ``min 9 10``.

Example:

```hs
succ 8
```

returns 9 because ``succ`` return the successor if the parameter has a defined one.

```hs
succ 9 * 10
```

Returns the successor of 9, then multiply it by 10.

```hs
succ (9 * 10)
```

Returns the successor of 90.

Every functions has to return something, in a 'if' statement, the 'else' is mandatory.  
You can use ``'`` as a valid function name character.

When a function doesn't have parameters, it's a definition.

```hs
doubleMe x = x + x
doubleUs x y = x*2 + y*2
doubleSmallNumber x = if x > 100
                        then x
                        else x*2
doubleSmallNumber' x = (if x > 100 then x else x*2) + 1
conanO'Brien = "It's a-me, Conan O'Brien!"
```

### Lists

Lists stores only element of the same type.

Put two lists together (works with strings as these are char lists): ``++``.

```hs
[1,2,3,4] ++ [9,10,11,12]
['w','o'] ++ ['o','t']
"hello" ++ " " ++ "world"
```

Put something at the beginning of a list: ``:``.

```hs
'A':" SMALL CAT"
```

Lists starts from 0, and you can get an element out of the list by index we can use ``!!``.  Lists can be compared if their elements can be compared too.

```hs
[3,2,1] > [2,1,0]
```

A few functions for the lists:

- ``head`` return the first element of a list given in parameter
- ``tail`` return all elements but the list's head for the given list in parameter
- ``last`` return the last element of the list given in parameter
- ``init`` return all elements but the last one for the list given in parameter
- ``length``
- ``null`` checks if the list given in parameter is null or not
- ``reverse`` reverses a list
- ``take`` needs a number n and a list l, and extract the n first elements of l
- ``drop`` needs a number n and a list l, and remove the n first elements of l
- ``maximum``
- ``minimum``
- ``sum``
- ``product``
- ``elem`` takes an element and a list of things and tells us if that thing is an element of the list
- ``cycle`` takes a list and cycle int infinitely (``take 10 (cycle [1, 2, 3])``)
- ``repeat`` takes an element and produce an infinite list only containing that element (``take 10 (repeat 5)``)
- ``replicate`` (``replicate 3 10`` > ``[10, 10, 10]``)

Ranges:

[1..10] is equivalent to [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].
['a'..'d'] is equivalent to "abcd".
[2,4..20] is equivalent to [2,4,6,8,10,12,14,16,18,20].
[3,6..20] is equivalent to [3,6,9,12,15,18].

> Be careful with ranges with floating numbers: [0.1, 0.3 .. 1] gives [0.1,0.3,0.5,0.7,0.8999999999999999,1.0999999999999999]

```hs
[x*2 | x <- [1..10]]
```

Returns

```hs
[2,4,6,8,10,12,14,16,18,20]
```

And

```hs
[ x*y | x <- [2,5,10], y <- [8,10,11]]
```

Returns

```hs
[16,20,22,40,50,55,80,100,110]
```

We can also use predicates:

```hs
ghci> [ x*y | x <- [2,5,10], y <- [8,10,11], x*y > 50]  
[55,80,100,110]

removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']]
```

### Tuples

A few fucntions:

- ``fst`` takes a par and returns its first component (only for pairs)
- ``snd`` same but the second component (only for pairs)
- ``zip`` produces a list of pairs from two lists (if length aren't the same, the longest one is cut).

## Types & Typeclasses

> Everything in Haskell has a given type.

### Types

To examine the type of something you can do ``:t``.  
``::`` means ``has type of``.  
``[Char]`` is a string.  
``:t`` on an expression gives something like that: ``removeNonUppercase :: [Char] -> [Char]`` meaning that it takes a String and maps it to a String.  
For a method with multiple parameters:

```hs
addThree :: Int -> Int -> Int -> Int  
addThree x y z = x + y + z
```

Types ***always*** starts with a capital letter. If not, then it's a type variable. Example:

```hs
ghci> :t fst  
fst :: (a, b) -> a

ghci> :t head
head :: [a] -> a
```

### Typeclasses

> A typeclass is a sort of interface that defines some behavior.

```hs
ghci> :t (==)  
(==) :: (Eq a) => a -> a -> Bool
```

Everything before the => symbol is called a class constraint. We can read the previous type declaration like this: the equality function takes any two values that are of the same type and returns a Bool. The type of those two values must be a member of the Eq class (this was the class constraint).

``Ord`̀ is for types that have an ordering.

```hs
ghci> :t (>)
(>) :: (Ord a) => a -> a -> Bool
```

``compare`` takes two Ord members of the same type and returns an ordering (``LT``, ``GT``, ``EQ``).

Example:

```hs
ghci> "Abrakadabra" < "Zebra"  
True  
ghci> "Abrakadabra" `compare` "Zebra"  
LT  
ghci> 5 >= 2  
True  
ghci> 5 `compare` 3  
GT
```

Members of ``Show`` can be represented as Strings using the ``show`` function. The opposite can be done using ``read`` on a ``Read`` member.

Using read sometimes give this if we don't give anything else to the GHCI interpreter.

```hs
ghci> read "4"  
<interactive>:1:0:  
    Ambiguous type variable `a' in the constraint:  
      `Read a' arising from a use of `read' at <interactive>:1:0-7  
    Probable fix: add a type signature that fixes these type variable(s)
```

This is because GHCI doesn't know what type to return. It can be fixed with:

```hs
ghci> read "5" :: Int  
5  
ghci> read "5" :: Float  
5.0  
ghci> (read "5" :: Float) * 4  
20.0  
ghci> read "[1,2,3,4]" :: [Int]  
[1,2,3,4]  
ghci> read "(3, 'a')" :: (Int, Char)  
(3, 'a')
```

There is also ``Enum`` members and ``Bounded`` ones. The ``Bounded`` members have an upper and a lower bound.

```hs
ghci> minBound :: Int  
-2147483648  
ghci> maxBound :: Char  
'\1114111'  
ghci> maxBound :: Bool  
True  
ghci> minBound :: Bool  
False
```

``Integral`` is a numeric typeclass that only includes integral numbers, it includes ``Ìnt`` and ``Integer``. ``Num`` on the other hand, includes all numbers, including real and integral ones. ``Floating`` includes only... floating numbers, so ``Float`` and ``Double``.

## Syntax in Functions

### Pattern Matching

```hs
lucky :: (Integral a) => a -> String  
lucky 7 = "LUCKY NUMBER SEVEN!"  
lucky x = "Sorry, you're out of luck, pal!"  

sayMe :: (Integral a) => a -> String  
sayMe 1 = "One!"  
sayMe 2 = "Two!"  
sayMe 3 = "Three!"  
sayMe 4 = "Four!"  
sayMe 5 = "Five!"  
sayMe x = "Not between 1 and 5"

factorial :: (Integral a) => a -> a  
factorial 0 = 1  
factorial n = n * factorial (n - 1)
```

Pattern matching can also fail:

```hs
charName :: Char -> String  
charName 'a' = "Albert"  
charName 'b' = "Broseph"  
charName 'c' = "Cecil"

ghci> charName 'a'  
"Albert"  
ghci> charName 'b'  
"Broseph"  
ghci> charName 'h'  
"*** Exception: tut.hs:(53,0)-(55,21): Non-exhaustive patterns in function charName
```

It can also be used on tuples:

```hs
addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)  
addVectors a b = (fst a + fst b, snd a + snd b)
```

The better way to do it is:

```hs
addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)  
addVectors (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)
```

If we don't care about what an element is we can use ``_``:

```hs
first :: (a, b, c) -> a  
first (x, _, _) = x  
  
second :: (a, b, c) -> b  
second (_, y, _) = y  
  
third :: (a, b, c) -> c  
third (_, _, z) = z
```

```hs
ghci> let xs = [(1,3), (4,3), (2,4), (5,3), (5,6), (3,1)]  
ghci> [a+b | (a,b) <- xs]  
[4,7,6,8,11,4]

head' :: [a] -> a  
head' [] = error "Can't call head on an empty list, dummy!"  
head' (x:_) = x

ghci> head' [4,5,6]  
4  
ghci> head' "Hello"  
'H'

tell :: (Show a) => [a] -> String  
tell [] = "The list is empty"  
tell (x:[]) = "The list has one element: " ++ show x  
tell (x:y:[]) = "The list has two elements: " ++ show x ++ " and " ++ show y  
tell (x:y:_) = "This list is long. The first two elements are: " ++ show x ++ " and " ++ show y
```

#### As patterns

You can also use things called ``as patterns`` using an ``@``. It prevents repeating the same thing in the code such as:

```hs
capital :: String -> String  
capital "" = "Empty string, whoops!"  
capital all@(x:xs) = "The first letter of " ++ all ++ " is " + [x]
ghci> capital "Dracula"  
"The first letter of Dracula is D"
```

#### Guards

```hs
bmiTell :: (RealFloat a) => a -> String  
bmiTell bmi  
    | bmi <= 18.5 = "You're underweight, you emo, you!"  
    | bmi <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | bmi <= 30.0 = "You're fat! Lose some weight, fatty!"  
    | otherwise   = "You're a whale, congratulations!"

bmiTell :: (RealFloat a) => a -> a -> String  
bmiTell weight height  
    | weight / height ^ 2 <= 18.5 = "You're underweight, you emo, you!"  
    | weight / height ^ 2 <= 25.0 = "You're supposedly normal Pffft, I bet you're ugly!"  
    | weight / height ^ 2 <= 30.0 = "You're fat! Lose some weight, fatty!"  
    | otherwise                 = "You're a whale, congratulations!"  
```

Notice that there is no ``=``.  Guards can also be written inline but at the cost of worse readability.

```hs
max' :: (Ord a) => a -> a -> a  
max' a b | a > b = a | otherwise = b

myCompare :: (Ord a) => a -> a -> Ordering  
a `myCompare` b  
    | a > b     = GT  
    | a == b    = EQ  
    | otherwise = LT  

ghci> 3 `myCompare` 2  
GT
```

```hs
bmiTell :: (RealFloat a) => a -> a -> String  
bmiTell weight height  
    | weight / height ^ 2 <= 18.5 = "You're underweight, you emo, you!"  
    | weight / height ^ 2 <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | weight / height ^ 2 <= 30.0 = "You're fat! Lose some weight, fatty!"  
    | otherwise                   = "You're a whale, congratulations!"
```

Is equivalent (but less elegant) to:

```hs
bmiTell :: (RealFloat a) => a -> a -> String  
bmiTell weight height  
    | bmi <= skinny = "You're underweight, you emo, you!"  
    | bmi <= normal = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | bmi <= fat    = "You're fat! Lose some weight, fatty!"  
    | otherwise     = "You're a whale, congratulations!"  
    where bmi = weight / height ^ 2  
          skinny = 18.5  
          normal = 25.0  
          fat = 30.0  
```

You can also use binding to pattern match:

```hs
where bmi = weight / height ^ 2  
      (skinny, normal, fat) = (18.5, 25.0, 30.0)
```

```hs
calcBmis :: (RealFloat a) => [(a, a)] -> [a]  
calcBmis xs = [bmi w h | (w, h) <- xs]  
    where bmi weight height = weight / height ^ 2
```

You can use ``let <bindings> in <expression>``.

#### Case expressions

```hs
head' :: [a] -> a  
head' xs = case xs of [] -> error "No head for empty lists!"  
                      (x:_) -> x

case expression of pattern -> result  
                   pattern -> result  
                   pattern -> result  
                   ...

describeList :: [a] -> String  
describeList xs = "The list is " ++ case xs of [] -> "empty."  
                                               [x] -> "a singleton list."
                                               xs -> "a longer list."

describeList :: [a] -> String  
describeList xs = "The list is " ++ what xs  
    where what [] = "empty."  
          what [x] = "a singleton list."  
          what xs = "a longer list."  
```

## Recursion

```hs
maximum' :: (Ord a) => [a] -> a  
maximum' [] = error "maximum of empty list"  
maximum' [x] = x  
maximum' (x:xs)
    | x > maxTail = x  
    | otherwise = maxTail  
    where maxTail = maximum' xs
```

We can even use ``max`` to make it clearer.

```hs
maximum' :: (Ord a) => [a] -> a  
maximum' [] = error "maximum of empty list"  
maximum' [x] = x  
maximum' (x:xs) = max x (maximum' xs)
```

Let's code our own implementation of ``replicate'`` that takes two arguments n and x and returns a list of n times x:

```hs
replicate' :: (Num i, Ord i) => i -> a -> [a]
replicate' n x
    | n <= 0 = []
    | otherwise = x:replicate' (n-1) x

take' :: (Num i, Ord i) => i -> [a] -> [a]  
take' n _  
    | n <= 0   = []  
take' _ []     = []  
take' n (x:xs) = x : take' (n-1) xs

reverse' :: [a] -> [a]  
reverse' [] = []  
reverse' (x:xs) = reverse' xs ++ [x]
```

Haskell also supports infinite lists:

```hs
repeat' :: a -> [a]  
repeat' x = x:repeat' x
```

```hs
zip' :: [a] -> [b] -> [(a,b)]  
zip' _ [] = []  
zip' [] _ = []  
zip' (x:xs) (y:ys) = (x,y):zip' xs ys
```

```hs
quicksort :: (Ord a) => [a] -> [a]  
quicksort [] = []  
quicksort (x:xs) =
    let smallerSorted = quicksort [a | a <- xs, a <= x]  
        biggerSorted = quicksort [a | a <- xs, a > x]  
    in  smallerSorted ++ [x] ++ biggerSorted

ghci> quicksort [10,2,5,3,1,6,7,4,2,3,4,8,9]  
[1,2,2,3,3,4,4,5,6,7,8,9,10]  
ghci> quicksort "the quick brown fox jumps over the lazy dog"  
"        abcdeeefghhijklmnoooopqrrsttuuvwxyz"
```

## Higher Order Functions

Functions can take other functions as parameters and return other functions. If a function does one or the other it's called a ``higher order function``.

In Haskell, functions only accept one parameter. All functions that accept more are called ``curried functions``.  These two are equivalent:

```hs
ghci> max 4 5  
5  
ghci> (max 4) 5  
5
```

### Maps and filters

A few functions:

- ``map`` takes a function and a list and applies this function to every element in the list
- ``filter`` takes a predicate (a function returning a boolean value) and a list and returns the list of elements that satisfy the predicate
- ``takeWhile`` takes a predicate and a list and return everything from the beginning of the list until the predicate isn't true anymore

### Lambdas

Just after the filter here, there is a lambda, instead of creating a function for the only purpose to pass it to filter:

```hs
numLongChains :: Int  
numLongChains = length (filter (\xs -> length xs > 15) (map chain [1..100]))
```

``foldl`` folds a list from the left. ``foldr`` does the same thing but from the right.

```hs
sum' :: (Num a) => [a] -> a  
sum' xs = foldl (\acc x -> acc + x) 0 xs
```

``scanr`` & ``scanl`` are like folds but they report the accumulator as a list:

```hs
ghci> scanl (flip (:)) [] [3,2,1]  
[[],[3],[2,3],[1,2,3]]
```

### Function application with $

```hs
($) :: (a -> b) -> a -> b  
f $ x = f x
```

In Haskell, a function is naturally left associative (``f a b c`` is the same as ``(((f a) b) c)``), and a function with $ is right associative (``f $ g $ z x`` is equal to ``f (g (z x))``).

#### Function Composition

In mathematics, it is defines like this: ``(f . g)(x) = f(g(x))``.

In Haskell it is defined like this:

```hs
(.) :: (b -> c) -> (a -> b) -> a -> c  
f . g = \x -> f (g x)
```

So these twos are equivalent:

```hs
ghci> map (\x -> negate (abs x)) [5,-3,-6,7,-3,2,-19,24]  
[-5,-3,-6,-7,-3,-2,-19,-24]
ghci> map (negate . abs) [5,-3,-6,7,-3,2,-19,24]  
[-5,-3,-6,-7,-3,-2,-19,-24]
```

``f (g (z x))`` is equivalent to ``(f . g . z) x``.  
So we can simplify things as:

```hs
ghci> map (\xs -> negate (sum (tail xs))) [[1..5],[3..6],[1..7]]  
[-14,-15,-27]
```

into:

```hs
ghci> map (negate . sum . tail) [[1..5],[3..6],[1..7]]  
[-14,-15,-27]
```

## Modules

### Loading modules

To import a module, simply do ``import <module name>``. For example, to import the *Data.List* module:

```hs
import Data.List  
  
numUniques :: (Eq a) => [a] -> Int  
numUniques = length . nub
```

When using GHCI, you can import it like that:

```hs
ghci> :m + Data.List
ghci> :m + Data.List Data.Map Data.Set
```

If you've loaded a script that already imports a module, there is no need to import it manually in GHCI.

If you only need specific functions from a module you can import them like that:

```hs
import Data.List (nub, sort)
```

Or if you want to import all functions but one:

```hs
import Data.List hiding (nub)
```

Maybe sometimes a module has the same functions as another one (Data.Map contains functions such as ``filter`` or ``null``, which have the same name as the one already imported in Prelude). When calling these functions, GHCI won't know which one is being called, so in order to fix that, you can do:

```hs
import qualified Data.Map
```

So instead of calling ``filter`` by its name, you can call it using the fully qualified name ``Data.Map.filter``.

You can also do:

```hs
import qualified Data.Map as M
```

So instead of calling ``Data.Map.filter`` you can do ``M.filter``.  You can search on [Hoogle](http://haskell.org/hoogle) to find some functions by name, module name or even signature.

### ``Data.List``

> You don't have to import ``Data.List`` with the qualified method as it has no function with the same name as another one in ``Prelude``.

Here are a few functions from the ``Data.List`` module:

- ``interperse`` takes an element and a list and puts that element between each pair of the list
- ``intercalate`` takes a list (l) and a list of lists (m) and insterts l between each par of lists in m and then flatten the result into one list
- ``transpose`` takes a list of lists and treats it as a 2D matrix, the columns become the rows and vice-versa
- ``concat`` flatten a list of lists into just a list of items
- ``concatMap`` is the same as firt mapping a function to a list and then concatenating the list with ``concat``
- ``and`` takes a list of boolean values and returns ``True`` only if all the values in the list are ``True`` (ie: ``and $ map (>4) [5,6,7,8]``)
- ``or`` does the same as ``and`` but with an ``or``
- ``any`` takes a predicate and check if any element in the list satisfies it
- ``all`` same as above but with all elements
- ``iterate`` takes a function and a starting value, then applies the function to the starting value, then to the result, then to the... It returns all the results in a form of an infinite list
- ``splitAt`` takes a number and a list and split the list at the position given in number
- ``takeWhile`` takes a predicate and a list and takes the elements in the list, from the beginning to the point where the predicate ins't satisfied anymore
- ``dropWhile`` is the same but drops/removes element from the list instead of taking them
- ``span`` does the same as ``takeWhile`` but returns a pair of lists: what is returned by ``takeWhile`` is in the first one, and the second part, what would have been dropped
- ``break`` where ``span`` spans the list while the predicate is satisfied, ``break`` breaks it when the predicate is first satisfied (``break p`` is the equivalent of ``span (not . p)``)
- ``sort`` simply sorts a list
- ``group`` takes a list and groups adjacents elements that are equals in sublists
- ``inits`` and ``tails`` are like ``init`` and ``tail`` but they recursively apply that to a list until there's nothing left
- ``isInfixOf`` searchis for a sublist within a list and returns ``True`` if the sublist we're looking for is somewhere in the target list
- ``isPrefixOf`` and ``isSuffixFor`` are like ``startsWith`` and ``endsWith`` in other languages
- ``elem`` and ``notElem`` check if an element is or isn't inside a list
- ``partition`` takes a predicate and a list and returns a pair of lists: the first one contains all elements that satify the predicate and the second the ones that don't
- ``find`` takes a list and a predicate and return the first element that satisfies the predicate wrapped in a ``Maybe`` value that can either be ``Just something`` or ``Nothing``. ``something`` has the same type as what is contained in the list
- ``elemIndex`` is like ``elem`` but it returns the index of the element in the list as a ``Maybe`` value
- ``elemIndices`` is like above but it returns a list of all the indices where the element is
- ``findIndex`` returns the first element that satisfies a predicate as a ``Maybe`` value
- ``zip3``, ``zipWith3``, ``zip4``, ``zipWith4`` (it goes up to 7) are like ``zip`` and ``zipWith`` but takes 3/4/5... lists.
- ``lines`` takes a string and returns every line of that string in elements of a list
- ``unlines`` does the opposite
- ``words`` and ``unwords`` are for splitting a line of text into words or joining a list of words into a text
- ``nub`` removes duplicates in a list
- ``delete`` takes an element and a list and... ***deletes*** the first occurence of this element from the list
- `̀\\`` is the list difference function
- ``union`` does the union between two lists
- ``intersect`` does an intersection
- ``insert`` inserts an element in a list just before it finds an element greater or equal

There are also more generic versions of these functions:

- ``genericLength``
- ``genericTake``
- ``genericDrop``
- ``genericSplitAt``
- ``genericIndex``
- ``genericReplicate``

And ``by`` functions:

- ``nubBy``
- ``deleteBy``
- ``unionBy``
- ``intersectBy``
- ``groupBy``
- ``sortBy``
- ``insertBy``
- ``maximumBy``
- ``minimumBy``

### ``Data.Char``

Here are a list of handy Data.Char functions:

- ``isControl``
- ``isSpace``
- ``isLower``/``isUpper``
- ``isAlpha``
- ``isAlphaNum``
- ``isPrint`` to check if a character is printable
- ``isDigit``/``isOctDigit``/``isHexDigit``
- ``isLetter``
- ``isMark``
- ``isNumber``
- ``isPunctuation``
- ``isSymbol``
- ``isSeparator``
- ``isAscii``/``isAsciiUpper``/``isAsciiLower``
- ``isLatin1``
- ``toUpper``/``toTitle``
- ``toLower``
- ``digitToInt``/``intToDigit`` (digit being an hex number)
- ``ord`` takes a letter/char and returns its corresponding number, whereas ``chr`` does the opposite
- ``encode`` takes a number as an offset and a string and shift the letters in the string by the offset

### [``Data.Map``](http://www.haskell.org/ghc/docs/latest/html/libraries/containers/Data-Map.html#v%3Aassocs)

> A map is like a list of pairs.

As it contains functions that can clash with others from ``Prelude`` it should be imported with ``import qualified Data.Map as Map``.

A few functions:

- ``fromList`` takes an association list and returns a map with the same associations, and ``toList`` does the opposite
- ``empty`` returns an empty list
- ``insert`` takes a key, a value and a map and returns a new map with the key-value inserted
- ``null`` checks if a map is empty or not
- ``size`` returns the size of a map
- ``singleton`` takes a key and a value and creates a map that has exactly one mapping
- ``lookup`` returns a ``Maybe`` value and works like ``Data.List.lookup``
- ``map`` and ``filter`` work like their list equivalents
- ``keys`` and ``elems`` returns a list of keys and elements
- ``fromListWith``
- ``insertWith``

### ``Data.Set``

As for ``Data.Map``, ``Data.Set`` should be imported qualified.

Here are some handy functions:

- ``fromList``
- ``intersection``
- ``difference``
- ``union``
- ``null``
- ``size``
- ``member``
- ``empty``
- ``singleton``
- ``insert``
- ``delete``
- ``map``
- ``filter``
- ``toList`` (it is faster to do ``let setNub xs = Set.toList $ Set.fromList xs`` instead of doing ``nub`` on a String)

### Making our own modules

Let's do a Geometry module, the file name should be the same as the module name (Geometry.hs).

It can be declared like that:

```hs
module Geometry  
( sphereVolume  
, sphereArea  
, cubeVolume  
, cubeArea  
, cuboidArea  
, cuboidVolume  
) where  
  
sphereVolume :: Float -> Float  
sphereVolume radius = (4.0 / 3.0) * pi * (radius ^ 3)  
  
sphereArea :: Float -> Float  
sphereArea radius = 4 * pi * (radius ^ 2)  
  
cubeVolume :: Float -> Float  
cubeVolume side = cuboidVolume side side side  
  
cubeArea :: Float -> Float  
cubeArea side = cuboidArea side side side  
  
cuboidVolume :: Float -> Float -> Float -> Float  
cuboidVolume a b c = rectangleArea a b * c  
  
cuboidArea :: Float -> Float -> Float -> Float  
cuboidArea a b c = rectangleArea a b * 2 + rectangleArea a c * 2 + rectangleAreac b * 2
  
rectangleArea :: Float -> Float -> Float  
rectangleArea a b = a * b
```

Then it can be imported doing ``import Geometry`̀ if the Geometry.hs file is in the same folder as we are.

Modules can also have a hierarchy. To do so, you create a Geometry folder and inside it you can create modules like Sphere.hs that contains:

```hs
module Geometry.Sphere  
( volume  
, area  
) where  
  
volume :: Float -> Float  
volume radius = (4.0 / 3.0) * pi * (radius ^ 3)  
  
area :: Float -> Float  
area radius = 4 * pi * (radius ^ 2)
```

and then import it doing ``import Geometry.Sphere``.

## Making your own types and typeclasses

In order to create our own data type, we have to use the ``data`` keyword.

Let's say a Shape can be a Circle or a Rectangle, we can then say:

```hs
data Shape = Circle Float Float Float | Rectangle Float Float Float Float
```

Let's take a look at what is a Circle and a Rectangle:

```hs
ghci> :t Circle  
Circle :: Float -> Float -> Float -> Shape  
ghci> :t Rectangle  
Rectangle :: Float -> Float -> Float -> Float -> Shape
```

Their constructors accept 3 and 4 Floats respectively and return a Shape object.

They can be used as follows:

```hs
surface :: Shape -> Float  
surface (Circle _ _ r) = pi * r ^ 2  
surface (Rectangle x1 y1 x2 y2) = (abs $ x2 - x1) * (abs $ y2 - y1)
```

To add the fact that this object can be converted to a String:

```hs
data Shape = Circle Float Float Float | Rectangle Float Float Float Float deriving (Show)
```

As value constructors are function, we can map them, partially apply them, and do this (to get a list of concentric circles):

```hs
ghci> map (Circle 10 20) [4,5,6,6]  
[Circle 10.0 20.0 4.0,Circle 10.0 20.0 5.0,Circle 10.0 20.0 6.0,Circle 10.0 20.0 6.0]
```

In order to export the functions and objects we can do:

```hs
module Shapes
( Point(..)  
, Shape(..)  
, surface  
, nudge  
, baseCircle  
, baseRect  
) where...
```

By doing ``Shape(...)`` we exported all value constructors for Shape.  That means that whoever imports our module can make shapes using the Rectangle and Circle value constructors. It's the same as writing ``Shape(Rectangle, Circle)``.

We could also not export everything by just writing ``Shape`` in the export statement, so the only way to create a Shape is by using ``baseCircle`` and ``baseRect``. The same mechanism is used in the ``Data.Map`` module where you can create a map using the ``Map.fromList`` function but not the ``Map.Map`` value constructor.

### Record Syntax

Let's make a Person object:

```hs
data Person = Person String String Int Float String String deriving (Show)
ghci> let guy = Person "Buddy" "Finklestein" 43 184.2 "526-2928" "Chocolate"  
ghci> guy  
Person "Buddy" "Finklestein" 43 184.2 "526-2928" "Chocolate"
```

As it is slightly unreadable we can do:

```hs
firstName :: Person -> String  
firstName (Person firstname _ _ _ _ _) = firstname  
  
lastName :: Person -> String  
lastName (Person _ lastname _ _ _ _) = lastname  
  
age :: Person -> Int  
age (Person _ _ age _ _ _) = age  
  
height :: Person -> Float  
height (Person _ _ _ height _ _) = height  
  
phoneNumber :: Person -> String  
phoneNumber (Person _ _ _ _ number _) = number  
  
flavor :: Person -> String  
flavor (Person _ _ _ _ _ flavor) = flavor
```

and then:

```hs
ghci> let guy = Person "Buddy" "Finklestein" 43 184.2 "526-2928" "Chocolate"  
ghci> firstName guy  
"Buddy"  
ghci> height guy  
184.2  
ghci> flavor guy  
"Chocolate"
```

It can be simplified a lot defining a Person object as follows:

```hs
data Person = Person { firstName :: String  
                     , lastName :: String  
                     , age :: Int  
                     , height :: Float  
                     , phoneNumber :: String  
                     , flavor :: String  
                     } deriving (Show)
```

So we could have:

```hs
ghci> :t flavor  
flavor :: Person -> String  
ghci> :t firstName  
firstName :: Person -> String
```

Let's define a car:

```hs
data Car = Car String String Int deriving (Show)
ghci> Car "Ford" "Mustang" 1967  
Car "Ford" "Mustang" 1967
```

Using the ***record syntax***:

```hs
data Car = Car {company :: String, model :: String, year :: Int} deriving (Show)
ghci> Car {company="Ford", model="Mustang", year=1967}  
Car {company = "Ford", model = "Mustang", year = 1967}
```

> When making a new car, we don't have to necessarily put the fields in the proper order, as long as we list all of them. But if we don't use record syntax, we have to specify them in order.

### Type parameters

> A value constructor can take some values parameters and then produce a new value. For instance, the Car constructor takes three values and produces a car value. In a similar manner, type constructors can take types as parameters to produce new types.

```hs
data Maybe a = Nothing | Just a
```

Using this concept we could change this:

```hs
data Car = Car { company :: String  
               , model :: String  
               , year :: Int  
               } deriving (Show)
```

into this:

```hs
data Car a b c = Car { company :: a  
                     , model :: b  
                     , year :: c
                     } deriving (Show)
```

### Derived Instances

Let's add a way to check if two persons are equals:

```hs
data Person = Person { firstName :: String  
                     , lastName :: String  
                     , age :: Int  
                     } deriving (Eq)
```

So now we can check for equality:

```hs
ghci> let mikeD = Person {firstName = "Michael", lastName = "Diamond", age = 43}  
ghci> let adRock = Person {firstName = "Adam", lastName = "Horovitz", age = 41}  
ghci> let mca = Person {firstName = "Adam", lastName = "Yauch", age = 44}  
ghci> mca == adRock  
False  
ghci> mikeD == adRock  
False  
ghci> mikeD == mikeD  
True  
ghci> mikeD == Person {firstName = "Michael", lastName = "Diamond", age = 43}  
True
ghci> let beastieBoys = [mca, adRock, mikeD]  
ghci> mikeD `elem` beastieBoys  
True
```

The ``Show`` and ``Read`` Typeclasses are for things that can be converted from or to a string:

```hs
data Person = Person { firstName :: String  
                     , lastName :: String  
                     , age :: Int  
                     } deriving (Eq, Show, Read)
```

Now we can do that:

```hs
ghci> let mikeD = Person {firstName = "Michael", lastName = "Diamond", age = 43}  
ghci> mikeD  
Person {firstName = "Michael", lastName = "Diamond", age = 43}  
ghci> "mikeD is: " ++ show mikeD  
"mikeD is: Person {firstName = \"Michael\", lastName = \"Diamond\", age = 43}"
ghci> read "Person {firstName =\"Michael\", lastName =\"Diamond\", age = 43}" :: Person  
Person {firstName = "Michael", lastName = "Diamond", age = 43}
ghci> read "Person {firstName =\"Michael\", lastName =\"Diamond\", age = 43}" == mikeD  
True
```

We can also derivate instances from the ``Ord`` typeclass:

```hs
data Bool = False | True deriving (Ord)
```

> Because the False value constructor is specified first and the True value constructor is specified after it, we can consider True as greater than False.

```hs
ghci> True `compare` False  
GT  
ghci> True > False  
True  
ghci> True < False  
False
```

We could create classes with the ``Bounded`` typeclass so we could have a lowest and highest value:

```hs
data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday
           deriving (Eq, Ord, Show, Read, Bounded, Enum)
```

So we could do:

```hs
ghci> Saturday == Sunday  
False  
ghci> Saturday == Saturday  
True  
ghci> Saturday > Friday  
True  
ghci> Monday `compare` Wednesday  
LT
ghci> minBound :: Day  
Monday  
ghci> maxBound :: Day  
Sunday
```

As it is an instance of ``Enum``, we can get predecessors and successors:

```hs
ghci> succ Monday  
Tuesday  
ghci> pred Saturday  
Friday  
ghci> [Thursday .. Sunday]  
[Thursday,Friday,Saturday,Sunday]  
ghci> [minBound .. maxBound] :: [Day]  
[Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday]
```

### Type synonyms

> Previously, we mentioned that when writing types, the [Char] and String types are equivalent and interchangeable. That's implemented with type synonyms. Type synonyms don't really do anything per se, they're just about giving some types different names so that they make more sense to someone reading our code and documentation. Here's how the standard library defines String as a synonym for [Char].

```hs
type String = [Char]
```

In order to define a PhoneBook we could do:

```hs
type PhoneBook = [(String,String)]
```

Or even

```hs
type PhoneNumber = String  
type Name = String  
type PhoneBook = [(Name,PhoneNumber)]

inPhoneBook :: Name -> PhoneNumber -> PhoneBook -> Bool  
inPhoneBook name pnumber pbook = (name,pnumber) `elem` pbook
```

Type synonyms could be even parametrized so it is more generic:

```hs
type AssocList k v = [(k,v)]
```

We can also partially apply functions with them:

```hs
type IntMap v = Map Int v
```

With that we could do:

```hs
import qualified Data.Map as Map  
  
data LockerState = Taken | Free deriving (Show, Eq)  
  
type Code = String  
  
type LockerMap = Map.Map Int (LockerState, Code)

lockerLookup :: Int -> LockerMap -> Either String Code  
lockerLookup lockerNumber map =
    case Map.lookup lockerNumber map of
        Nothing -> Left $ "Locker number " ++ show lockerNumber ++ " doesn't exist!"  
        Just (state, code) -> if state /= Taken
                                then Right code  
                                else Left $ "Locker " ++ show lockerNumber ++ " is already taken!"

lockers :: LockerMap  
lockers = Map.fromList
    [(100,(Taken,"ZD39I"))  
    ,(101,(Free,"JAH3I"))  
    ,(103,(Free,"IQSA9"))  
    ,(105,(Free,"QOTSA"))  
    ,(109,(Taken,"893JJ"))  
    ,(110,(Taken,"99292"))  
    ]
```

### Typeclasses 102

The ``Eq`` typeclass is for stuff that can be equated, and defines the function ``/=`` and ``==``.

```hs
class Eq a where  
    (==) :: a -> a -> Bool  
    (/=) :: a -> a -> Bool  
    x == y = not (x /= y)  
    x /= y = not (x == y)
```

Then we can have stuff like:

```hs
data TrafficLight = Red | Yellow | Green
```

```hs
instance Eq TrafficLight where  
    Red == Red = True  
    Green == Green = True  
    Yellow == Yellow = True  
    _ == _ = False
```

> Because == was defined in terms of /= and vice versa in the class declaration, we only had to overwrite one of them in the instance declaration. That's called the minimal complete definition for the typeclass — the minimum of functions that we have to implement so that our type can behave like the class advertises. To fulfill the minimal complete definition for Eq, we have to overwrite either one of == or /=.

Let's code the instance of ``Show``:

```hs
instance Show TrafficLight where  
    show Red = "Red light"  
    show Yellow = "Yellow light"  
    show Green = "Green light"
```

## Input and Output

### Hello World

```hs
main = putStrLn "hello, world"
```

Let's compile it:

```hs
$ ghc --make helloworld  
[1 of 1] Compiling Main             ( helloworld.hs, helloworld.o )  
Linking helloworld ...
$ ./helloworld  
hello, world
```

Let's examine what signature has ``putStrLn``:

```hs
ghci> :t putStrLn  
putStrLn :: String -> IO ()  
ghci> :t putStrLn "hello, world"  
putStrLn "hello, world" :: IO ()
```

> We can read the type of putStrLn like this: putStrLn takes a string and returns an I/O action that has a result type of () (i.e. the empty tuple, also know as unit). An I/O action is something that, when performed, will carry out an action with a side-effect (that's usually either reading from the input or printing stuff to the screen) and will also contain some kind of return value inside it. Printing a string to the terminal doesn't really have any kind of meaningful return value, so a dummy value of () is used.

```hs
main = do  
    putStrLn "Hello, what's your name?"  
    name <- getLine  
    putStrLn ("Hey " ++ name ++ ", you rock!")
```

Let's take a look at ``getLine``:

```hs
ghci> :t getLine  
getLine :: IO String
```

It is an IO action that returns a String. Here, ``getLine`` returns a String that we bind to ``name``.

> You can think of an I/O action as a box with little feet that will go out into the real world and do something there (like write some graffiti on a wall) and maybe bring back some data. Once it's fetched that data for you, the only way to open the box and get the data inside it is to use the <- construct. And if we're taking data out of an I/O action, we can only take it out when we're inside another I/O action.

In order to use other functions (like ``String -> String``) we could do:

```hs
main = do  
    putStrLn "Hello, what's your name?"  
    name <- getLine  
    putStrLn $ "Read this carefully, because this is your future: " ++ tellFortune name
```

The following code doesn't work:

```hs
nameTag = "Hello, my name is " ++ getLine
```

Because the ``++`` operator append two same type lists: but ``getLine`` has a type of ``IO String``.  Instead we could do that:

```hs
main = do  
    foo <- putStrLn "Hello, what's your name?"  
    name <- getLine  
    putStrLn ("Hey " ++ name ++ ", you rock!")
```

```hs
import Data.Char  
  
main = do  
    putStrLn "What's your first name?"  
    firstName <- getLine  
    putStrLn "What's your last name?"  
    lastName <- getLine  
    let bigFirstName = map toUpper firstName  
        bigLastName = map toUpper lastName  
    putStrLn $ "hey " ++ bigFirstName ++ " " ++ bigLastName ++ ", how are you?"
```

```hs
main = do
    line <- getLine  
    if null line  
        then return ()  
        else do  
            putStrLn $ reverseWords line  
            main  
  
reverseWords :: String -> String
reverseWords = unwords . map reverse . words
```

```hs
main = do  
    return ()  
    return "HAHAHA"  
    line <- getLine  
    return "BLAH BLAH BLAH"  
    return 4  
    putStrLn line
```

> All these returns do is that they make I/O actions that don't really do anything except have an encapsulated result and that result is thrown away because it isn't bound to a name. We can use return in combination with <- to bind stuff to names.

```hs
main = do  
    a <- return "hell"  
    b <- return "yeah!"  
    putStrLn $ a ++ " " ++ b
```

Let's examine some functions:

- ``putStr`` is like ``putStrLn`` but it doesn't jump to the line at the end of the String
- ``putChar`` will print a single char
- ``print`` will print anything that is an instance of ``Show``
- ``getChar`` will get one char from the input
- ``sequence`` takes a list of IO actions and returns an IO action that will perform them one after the other
- ``forever`` takes an IO ation and returns an IO action that repeats the IO action it got forever

```hs
main = do  
    a <- getLine  
    b <- getLine  
    c <- getLine  
    print [a,b,c]
```

Doing that is same as:

```hs
main = do  
    rs <- sequence [getLine, getLine, getLine]  
    print rs
```

> The when function is found in Control.Monad (to get access to it, do import Control.Monad). It's interesting because in a do block it looks like a control flow statement, but it's actually a normal function. It takes a boolean value and an I/O action if that boolean value is True, it returns the same I/O action that we supplied to it. However, if it's False, it returns the return (), action, so an I/O action that doesn't do anything.

### Filters and Streams

``getContents`` is an IO action that reads everything from the standard input until it encounters an EOF character.

So using that we could turn

```hs
import Control.Monad  
import Data.Char  
  
main = forever $ do  
    putStr "Give me some input: "  
    l <- getLine  
    putStrLn $ map toUpper l
```

Into

```hs
import Data.Char  
  
main = do  
    contents <- getContents  
    putStr (map toUpper contents)
```

``interact`` takes a function of ``String -> String`` in parameter and returns an IO action that will take some input, run that function on it and print out the function's result.

```hs
main = interact shortLinesOnly  
  
shortLinesOnly :: String -> String  
shortLinesOnly input =
    let allLines = lines input  
        shortLines = filter (\line -> length line < 10) allLines  
        result = unlines shortLines  
    in  result
```

```hs
respondPalindromes = unlines . map (\xs -> if isPalindrome xs then "palindrome" else "not a palindrome") . lines  
    where   isPalindrome xs = xs == reverse xs

main = interact respondPalindromes
```

### File interactions

Let's read some files:

```hs
import System.IO  
  
main = do  
    handle <- openFile "girlfriend.txt" ReadMode  
    contents <- hGetContents handle  
    putStr contents  
    hClose handle
```

We could even use ``withFile`` to make it simple:

```hs
main = do
    withFile "girlfriend.txt" ReadMode (\handle -> do  
        contents <- hGetContents handle
        putStr contents)
```

We can use:

- ``readFile``
- ``writeFile``
- ``appendFile``
- ``openTempFile``
- ``removeFile``
- ``renameFile``
- ``doesFileExist``

### CLI arguments

``getArgs`` returns an ``IO [String]``, and ``getProgName`` returns an ``IO String``, and they can be used like that:

```hs
import System.Environment
import Data.List  
  
main = do  
   args <- getArgs  
   progName <- getProgName  
   putStrLn "The arguments are:"  
   mapM putStrLn args  
   putStrLn "The program name is:"  
   putStrLn progName
```

With that we could do a Task manager system:

```hs
import System.Environment
import System.Directory  
import System.IO  
import Data.List  
  
dispatch :: [(String, [String] -> IO ())]  
dispatch =  [ ("add", add)  
            , ("view", view)  
            , ("remove", remove)  
            ]  

main = do  
    (command:args) <- getArgs  
    let (Just action) = lookup command dispatch  
    action args  
  
add :: [String] -> IO ()  
add [fileName, todoItem] = appendFile fileName (todoItem ++ "\n")  
  
view :: [String] -> IO ()  
view [fileName] = do  
    contents <- readFile fileName  
    let todoTasks = lines contents  
        numberedTasks = zipWith (\n line -> show n ++ " - " ++ line) [0..] todoTasks  
    putStr $ unlines numberedTasks  
  
remove :: [String] -> IO ()  
remove [fileName, numberString] = do  
    handle <- openFile fileName ReadMode  
    (tempName, tempHandle) <- openTempFile "." "temp"  
    contents <- hGetContents handle  
    let number = read numberString  
        todoTasks = lines contents  
        newTodoItems = delete (todoTasks !! number) todoTasks  
    hPutStr tempHandle $ unlines newTodoItems  
    hClose handle  
    hClose tempHandle  
    removeFile fileName  
    renameFile tempName fileName
```
